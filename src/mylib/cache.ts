/**
 * It will loop N times only first-time searching, then every next-time searching will be only O(1)
 * @example USAGE
 * ```ts
 * const persons = [{ id: 'PS-001', name: 'John' },  { id: 'PS-002', name: 'Jane' }, { id: 'PS-003', name: 'Josh' }]
 *
 * findDataByDataID(persons, 'PS-002') // O(n)
 * findDataByDataID(persons, 'PS-001') // O(1)
 * findDataByDataID(persons, 'PS-001') // O(1)
 * findDataByDataID(persons, 'PS-003') // O(1)
 * findDataByDataID(persons, 'PS-002') // O(1)
 * ```
 */
export const findDataByDataID = (function memoization() {
  const cache: Record<string | number, any> = {}
  // let countLoop: number = 0
  return function findDataByDataID<T extends Record<string | number, any>>(data: T[], id: string | number): T {
    if (cache[id]) return cache[id]

    for (const item of data) {
      cache[item.id] = item
      // console.log(++countLoop, cache)
    }

    return cache[id]
  }
})()
