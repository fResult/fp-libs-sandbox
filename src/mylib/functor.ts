type AnyFunction = (...args: any[]) => any

// function Just<T>(v: T) {
//   return {
//     value: v,
//     map(f: AnyFunction) {
//       return Just(f(v))
//     }
//   }
// }

// function Nothing() {
  // return {
    // value: null,
    // map(f: AnyFunction) {
      // return Just(null)
    // }
  // }
// }

// const three = Just(3) //?

// const add2 = add(2)

// const test = three.map(add2) //?

function add(y: number) {
  return function forX(x: number) {
    return x + y
  }
}

export function Maybe<T>(x: T) {
  return {
    value: x,
    map(fn: (...args: any[]) => any) {
      return Maybe(
        x !== null ? fn(x) : null
      )
    },
    isNothing() {
      return x === null
    }
  }
}
