import { Many } from 'lodash'
import { compose } from './compose'

declare namespace _ {
  interface MyFpImpl {
    compose: FlowRight
  }

  interface FlowRight {
    <A extends any[], R1, R2, R3, R4, R5, R6, R7>(
      f7: (a: R6) => R7,
      f6: (a: R5) => R6,
      f5: (a: R4) => R5,
      f4: (a: R3) => R4,
      f3: (a: R2) => R3,
      f2: (a: R1) => R2,
      f1: (...args: A) => R1
    ): (...args: A) => R7
    <A extends any[], R1, R2, R3, R4, R5, R6>(
      f6: (a: R5) => R6,
      f5: (a: R4) => R5,
      f4: (a: R3) => R4,
      f3: (a: R2) => R3,
      f2: (a: R1) => R2,
      f1: (...args: A) => R1
    ): (...args: A) => R6
    <A extends any[], R1, R2, R3, R4, R5>(
      f5: (a: R4) => R5,
      f4: (a: R3) => R4,
      f3: (a: R2) => R3,
      f2: (a: R1) => R2,
      f1: (...args: A) => R1
    ): (...args: A) => R5
    <A extends any[], R1, R2, R3, R4>(
      f4: (a: R3) => R4,
      f3: (a: R2) => R3,
      f2: (a: R1) => R2,
      f1: (...args: A) => R1
    ): (...args: A) => R4
    <A extends any[], R1, R2, R3>(
      f3: (a: R2) => R2,
      f2: (a: R1) => R2,
      f1: (...args: A) => R1
    ): (...args: A) => R3
    <A extends any[], R1, R2>(f2: (a: R1) => R2, f1: (...args: A) => R1): (
      ...args: A
    ) => R2
    (...func: Array<Many<(...args: any[]) => any>>): (...args: any[]) => any
  }
}

export default class MyFp implements _.MyFpImpl {
  compose: _.FlowRight = compose
}
