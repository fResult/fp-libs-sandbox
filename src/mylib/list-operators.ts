type PredicateFunction = (...args: any[]) => boolean

export function reduce<T, U>(reducerFn: (acc: U, item: T, idx?: number) => U, initialValue: U) {
  return function forArray(arr: T[]) {
    return arr.reduce(reducerFn, initialValue!)
  }
}

export function filter<TPredicate extends PredicateFunction>(predicateFn: TPredicate) {
  return function fromArray<T extends Parameters<TPredicate>[0]>(arr: T[]) {
    return arr.filter(predicateFn)
  }
}

export function map<T, S>(mapperFn: (item: T, idx?: number) => S) {
  return function forArray(arr: any[]) {
    return arr.map(mapperFn)
  }
}

/**
 * @example
 * zip([1, 3, 5, 7, 9], [2, 4, 6, 8, 10]) // [[1, 2], [3, 4], [5, 6], [7, 8], [9, 10]]
 * zip([1, 3, 5, 7], [2, 4, 6, 8, 10])    // [[1, 2], [3, 4], [5, 6], [7, 8]]
 * zip([1, 3, 5], [2, 4, 6, 8, 10])       // [[1, 2], [3, 4], [5, 6]]
 */
export function zip<T, U>(arr1: T[], arr2: U[]): [T, U][] {
  const zipped: [T, U][] = []
  arr1 = arr1.slice()
  arr2 = arr2.slice()

  while (arr1.length > 0 && arr2.length > 0) {
    zipped.push([arr1.shift()!, arr2.shift()!])
  }

  return zipped
}

/**
 * @example
 * mergeLists([1, 3, 5, 7, 9], [2, 4, 6, 8, 10]) // [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
 * mergeLists([1, 3, 5, 7], [2, 4, 6, 8, 10])    // [1, 2, 3, 4, 5, 6, 7, 8, 10]
 * mergeLists([1, 3, 5], [2, 4, 6, 8, 10])       // [1, 2, 3, 4, 5, 6, 8, 10]
 */
export function mergeLists<T, U>(arr1: T[], arr2: U[]): (T | U)[] {
  const merged: (T | U)[] = []
  arr1 = arr1.slice()
  arr2 = arr2.slice()

  while (arr1.length > 0 || arr2.length > 0) {
    if (arr1.length > 0) {
      merged.push(arr1.shift()!)
    }
    if (arr2.length > 0) {
      merged.push(arr2.shift()!)
    }
  }

  return merged
}
