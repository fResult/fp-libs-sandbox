type AnyFunction = (...args: any[]) => any
type PredicateFunction = (...args: any[]) => boolean

/**
 * @description `not` is a function which input any function return boolean and negated its output
 * @example
 * ```typescript
 * function isOdd (x: number) {
 *   // the possible value of `x % 2` is `0` or `1`
 *   return x % 2 === 1
 * }
 * const isEven = not( isOdd )
 *
 * console.log( isOdd(3) ) // true
 * console.log( isOdd(4) ) // false
 *
 * console.log( isEven(3) ) // false
 * console.log( isEven(4) ) // true
 *
 * console.log( not( isOdd )(3) ) // false -> same `isEven(3)`
 * console.log( not( isOdd )(4) ) // true -> same `isEven(4)`
 * ```
 */
 export function not<TPredicateFn extends PredicateFunction>(predicateFn: TPredicateFn) {
  return function negated(...args: Parameters<TPredicateFn>) {
    return !predicateFn(...args)
  }
}


/**
 * @param fn - Any function that need to convert to *unary* function
 * @returns *unary* function (one parameter function)
 * @description `unary` is use to make function, then it can support point-free which 1 parameter
 *
 * @example
 * ```typescript
 * // ! DON'T
 * console.log(['1', '2', '3'].map( parseInt )) // [1, NaN, NaN]
 *
 * // DO
 * const convertStrToInt = unary(parseInt)
 * console.log(['1', '2', '3'].map( convertStrToInt )) // [1, 2, 3]
 *
 * // COULD ALSO DO, but not be quite recommended, coz readability is **lower** than above
 * console.log(['1', '2', '3'].map( unary( parseInt ) )) // also [1, 2, 3]
 * ```
 * @see Ref: {@link https://en.wikipedia.org/wiki/Arity#Unary **Function's arity - Unary** on Wikipedia}
 * @see Ref: {@link https://medium.com/p/b21a1416ac6a **Functional JS #7: Point-free style** article on Medium.com}
 */
export function unary<TFunction extends AnyFunction>(fn: TFunction) {
  return function appliedUnary(arg: Parameters<TFunction>[0]): ReturnType<TFunction> {
    return fn(arg)
  }
}

export function head<T>(arr: T[]) {
  return arr[0]
}

export function tail<T>(arr: T[]) {
  return arr.slice(1)
}
