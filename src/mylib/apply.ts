import {
  isAlpha,
  isNumeral,
  isSpace,
  isVowel
} from './predicate'

type PredicateFunction = (...args: any[]) => boolean

export function apply<T>(thing: T) {
  return function forPredicates(predicates: PredicateFunction[]) {
    // FIXME: logic is not functional
    return predicates.map(pred => pred(thing)).every(x => !!x)
  }
}

console.log(apply('a')([isVowel, isAlpha, isSpace, isNumeral]))
isSpace(' ') //?
isNumeral('9') //?

