import { apply } from "./apply";
import { not } from "./operators";

export function isVowel(char: string): boolean {
  char = char.toLowerCase();
  return ['a', 'e', 'i', 'o', 'u'].includes(char)
}

export function isUpper(char: string): boolean {
  return /[A-Z]/.test(char)
}

export function isLower(char: string): boolean {
  return /[a-z]/.test(char)
}

export function isAlphaNumeral(thing: unknown): boolean {
  // if (apply(thing)([not(isAlpha), not(isNumeral)]))
  if (!(typeof thing === 'string' || typeof thing === 'number'))
    return false
  return !Object.is(Number(thing), NaN)
}

export function isUndefined<T>(thing: T): thing is T {
  return typeof thing === 'undefined'
}

export function isAlpha<T>(thing: T): boolean {
  if (typeof thing !== 'string') return false
  return isUpper(thing) || isLower(thing)
}

export function isSpace<T>(thing: T): boolean {
  return thing === ' '
}

export function isNumeral<T>(thing: T): boolean {
  return typeof thing === 'number'
}