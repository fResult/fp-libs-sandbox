import { not } from "./operators";
import {
  isAlphaNumeral,
  isUndefined,
  isUpper,
  isVowel
} from './predicate'

type PredicateFunction = (...args: any[]) => boolean

export function countIf<
  TPredicate extends PredicateFunction,
  T extends Parameters<TPredicate>[0]
>(predicateFn: TPredicate): (things: T[] | string) => number {
  return function fromValue(xs: T[] | string) {
    if (typeof xs === 'string')
      xs = xs.split('') as T[]
    return filter(predicateFn)(xs).length;
  }
}

export function filter<TPredicate extends PredicateFunction>(predicateFn: TPredicate) {
  return function fromArray<T extends Parameters<TPredicate>[0]>(arr: T[]) {
    return arr.filter(predicateFn)
  }
}

// TRY TO USE
const countIfVowel = countIf(isVowel)
const countIfUpper = countIf(isUpper)
const countIfNumber = countIf(isAlphaNumeral)
const countIfDefined = countIf(not(isUndefined))

console.log('count vowel', countIfVowel('I have a pen')) //?

console.log(
  'count upper',
  countIfUpper('I have a pen, And the pen is red.')
) //?
console.log('count number', countIfNumber([1, "3", {}, [], new Date(), { id: 123 }, [2,3,4], 5, 0, '0'])); //?

console.log(
  'count defined value',
  countIfDefined(['Korn', 0, '', undefined, null, '0', null, [], {}])
) //?

const filterByDefined = filter(not(isUndefined))
console.log(filterByDefined(['a', 'e', 'i', 'o', 'f', undefined, null])) //?
