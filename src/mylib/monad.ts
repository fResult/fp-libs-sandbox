type IdentityFn<T> = (x: T) => T
type MapperFn<T, U> = (x: T) => U

function Just<TValue>(val: TValue) {
  return {
    map<T extends TValue, U>(fn: MapperFn<T, U>) {
      return Just(fn(val as T)) as unknown as ReturnType<typeof Just<U>>
    },
    chain<T extends TValue>(fn: IdentityFn<T>) {
      return fn(val as T)
    },
    ap(anotherMonad: any) {
      return anotherMonad.map( val )
    },
    inspect(): string {
      return `Just(${ val })`;
    }
  }
}

const justK = Just("K")
const justK33 = justK.map(x => x + 33)
const inspectedK33 = justK33.inspect()
const k33 = justK33.chain(x => x)
console.log(k33, inspectedK33) // "K33" "Just(K33)"
