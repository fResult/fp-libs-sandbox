export function useReactive() {
  interface IObserver {
    execute(): void
    dependencies: Set<Set<IObserver>>
  }
  let context: IObserver[] = []

  function untrack<R>(fn: () => R): R {
    const prevContext = context
    context = []
    const res = fn()
    context = prevContext
    return res
  }

  function cleanup(observer: IObserver): void {
    observer.dependencies.forEach(dep => {
      dep.delete(observer)
    })
    observer.dependencies.clear()
  }

  function subscribe(observer: IObserver, subscriptions: Set<IObserver>): void {
    subscriptions.add(observer)
    observer.dependencies.add(subscriptions)
  }

  function createSignal<T>(val?: T): [() => T, (newVal: T) => void] {
    const subscriptions = new Set<IObserver>()

    function read(): T {
      const observer = context.at(-1)
      if (observer) subscribe(observer, subscriptions)
      return val!
    }
    function write(newVal: T): void {
      val = newVal
      ;[...subscriptions].forEach(observer => {
        observer.execute()
      })
    }
    return [read, write]
  }

  function createEffect(fn: () => void): void {
    const effect: IObserver = {
      execute() {
        cleanup(effect)
        context.push(effect)
        fn()
        context.pop()
      },
      dependencies: new Set()
    }
    effect.execute()
  }

  function createMemo<R>(fn: () => R) {
    const [signal, setSignal] = createSignal<R>(fn())
    createEffect(() => setSignal(fn()))
    return signal
  }

  return { createEffect, createMemo, createSignal, untrack }
}

(function testSignal() {
  const { createEffect, createMemo, createSignal, untrack } = useReactive()

  const [count, setCount] = createSignal(0)
  const [count2, setCount2] = createSignal(2)
  const [show, setShow] = createSignal(true)

  const sum = createMemo(() => count() + count2())

  createEffect(() => {
    // if (show()) console.log(count())
    // else console.log(count2())
    console.log(count(), count2(), sum())
    console.log(untrack(() => count()))
    console.log(count(), count2())
  })

  // setCount2(3)
  // for (let i = 0;i<3;i++) {
  //   setCount(count() + 1)
  //   setCount2(count2() + 2)
  //   setShow(!show())
  // }
  // setShow(false)
  setCount(10)
  setCount2(20)
})()
