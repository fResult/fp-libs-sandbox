type ComposedFn<T, R> = (x: T) => R

/**
 * @example
 * function sum(x: number, y: number) {
 *   return x + y
 * }
 * function double(x: number) {
 *   return x * 2
 * }
 * function isOdd(x: number) {
 *   return x % 2 === 1
 * }
 * const composedFn = compose(
 *   reduce(sum, 0),
 *   map(double),
 *   filter(isOdd)
 * )
 * composedFn([1,2,3,4,5]) // 18
 */
export function compose<T1, T2, R>(f2: (a2: T2) => R, f1: (a1: T1) => T2): ComposedFn<T1, R>
export function compose<T1, T2, T3, R>(f3: (a3: T3) => R, f2: (a2: T2) => T3, f1: (a1: T1) => T2): ComposedFn<T1, R>
export function compose<T1, T2, T3, T4, R>(f4: (a4: T4) => R, f3: (a3: T3) => T4, f2: (a2: T2) => T3, f1: (a1: T1) => T2): ComposedFn<T1, R>
export function compose<T1, T2, T3, T4, T5, R>(f5: (a5: T5) => R, f4: (a4: T4) => T5, f3: (a3: T3) => T4, f2: (a2: T2) => T3, f1: (a1: T1) => T2): ComposedFn<T1, R>
export function compose<T1, T2, T3, T4, T5, T6, R>(f6: (a6: T6) => R, f5: (a5: T5) => T6, f4: (a4: T4) => T5, f3: (a3: T3) => T4, f2: (a2: T2) => T3, f1: (a1: T1) => T2): ComposedFn<T1, R>
export function compose(...fns: any[]) {
  return function composed(result: any) {
    const list = [...fns]
    while (list.length > 0) {
      result = list.pop()?.(result)
    }

    return result
  }
}
function prop(name: string, obj: Record<string | number, unknown>) {
  return obj[name];
}
