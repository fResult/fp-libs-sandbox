type Many<T> = T | ReadonlyArray<T>

type PredicateFunction = (...args: any[]) => boolean
type AnyFunction = (...args: any[]) => any

type Primitive = string | number | boolean | undefined | null

type DeepReadonly<T> = T extends Primitive ? T : {
    readonly [P in keyof T]: DeepReadonly<T[P]>
}

/**
 * Use opsosite for `DeepReadonly` utility type
 */
type DeepWritable<T> = T extends Primitive ? T : {
    -readonly [P in keyof T]: DeepWritable<T[P]>
}


// interface Person {
//     name: string;
//     address: Address
// }
// interface Address {
//     street: string;
//     district: string
// }

// const arr: DeepReadonly<Person[]> = [
//     {
//         name: 'Korn',
//         address: {
//             street: 'Sukhumvit',
//             district: 'Phrakhanong'
//         }
//     }
// ]

// arr[0] = {
//     name: '',
//     address: {
//         district: '',
//         street: ''
//     }
// }
// arr[0].name = 'korn'
