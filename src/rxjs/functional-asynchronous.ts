import * as Rx from 'rxjs'
import { ajax } from 'rxjs/ajax'

(
  /**
   * Subject is combined functionality from Observer and Observable
   */
  function Subject() {
  const a = new Rx.Subject<number>()

  function double(num: number) {
    return num * 2
  }

  (function closureFn() {
    let count = 0;
    const intervalID = setInterval(function everySecond () {
      a.next(++count)
      if (count >= 5) {
        clearInterval(intervalID)
      }
    }, 1000)
  })()

  const b = a.pipe(Rx.filter(x => x % 2 === 0), Rx.map(double))

  b.subscribe(function onValue(num) {
    console.log(num)
  })
})();

(
  /**
   * Keep the Observer and Observable separate by this...
   */
  function ObserverAndObservable (){
    const a$ = new Rx.Observable<number>(function onObserve(observer) {
      (function closureFn() {
        let count = 0
        const intervalID = setInterval(function everySecond() {
          observer.next(++count)

          if (count >= 5) {
            clearInterval(intervalID)
          }
        }, 1000)
      })()
    })

    const b$ = a$.pipe(
      Rx.filter(x => x % 2 === 1),
      Rx.distinctUntilChanged(),
      Rx.throttleTime(1000),
      Rx.map(x => x * 2)
    )

    b$.subscribe(function onValue(num){
      console.log("Next:", num)
    })
  }
)()
    // const objList$ = ajax.getJSON<{id: string}[]>('')
    // objList$.pipe(Rx.map(objList => {
      // objList.map(obj => obj.id)
    // }))