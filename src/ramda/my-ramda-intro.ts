import R from 'ramda'
;(() => {
  const sum = R.reduce(
    (acc, num) => {
      return acc + num
    },
    0,
    [1, 2, 3]
  )
  console.log({ sum })

  const f = R.curryN(3, function calculate(z: number, y: number, x: number) {
    console.debug(x, y, z)
    return x + y * z
  })
  const g = R.compose(R.inc, f<number[], [x: number], number>(4, 2))
  console.log(g(10))
})()
;(() => {
  type MapperFn<T, R> = (value: T, idx?: number) => R
  type FpMap = <T, R>(mapperFn: MapperFn<T, R>) => (arr: T[]) => R[]
  type ReducerFn<T, R> = (acc: R, value: T) => R
  type FpReduce = <T, R>(reducerFn: ReducerFn<T, R>) => (initialValue: R) => (arr: T[]) => R
  type PredicateFn<T> = (value: T) => boolean
  type FpFilter = <T>(predicateFn: PredicateFn<T>) => (arr: T[]) => T[]
  const map: FpMap =  R.invoker(1, 'map')
  const reduce: FpReduce = R.invoker(2, 'reduce')
  const filter: FpFilter = R.invoker(1, 'filter')
  console.log(map<number, number>((x) => x * 2)([1, 2, 3])) //?
  console.log(reduce<number, number>((acc, num) => acc + num)(0)([1,2,3]))
  console.log(filter<number>(x => x % 2 === 0)([1,2,3,4]))
  const add10 = R.curryN(1, R.add(10))
  const composedFn = R.compose(
    add10,
    reduce<number, number>(function sumReducer(acc, num) { return acc + num })(2),
    map<number, number>(function pow2Mapper(x) { return x ** 2 }),
    filter<number>(function isOdd(x) { return x % 2 === 1 })
  )
  console.log(composedFn([1, 2, 3, 4, 5]))
})()

