import fp from 'lodash/fp'
import _ from 'lodash'
;(() => {
  /*
   * NOTE: http://static.frontendmasters.com/resources/2019-05-06-functional-light-v3/functional-light-v3.pdf
   * Slide 180
   */
  ;(() => {
    const kyle = "Kyle's example"
    console.log(
      kyle + 1,
      fp.reduce((acc, v) => acc + v, 0, [3, 7, 9])
    )

    /**
     * @example
     * ```ts
     * function ff(z: number) {
     *   return function forY(y: number) {
     *     return function forX(x: number) {
     *       return x + y + z
     *     }
     *   }
     * }
     * ```
     */
    const f = fp.curryN(3, function f(z: number, y: number, x: number) {
      console.debug(z, y, x)
      return x + y * z
    })
    const mult2 = f(2)
    const plus3Mult2 = mult2(3)
    const plus3Mult2By5 = plus3Mult2(5)
    console.log(plus3Mult2By5)

    const g = fp.curryN(2, f(4))
    const h = fp.compose(fp.add(1), g(3))
    console.log(kyle + 2, h(2))
  })()
  ;(() => {
    const mapper = fp.map<number, number>(fp.add(1))
    console.log(mapper([1, 2, 3]))
    const add5 = fp.add(5)
    const add10 = fp.add(10)
    const add15 = fp.add(15)
    const added20And15 = add15(20)
    const added10And15And20 = add10(added20And15)
    const added5And10And15And20 = add5(added10And15And20)
    console.log(added5And10And15And20)
    console.log(add5(add10(add15(20))))
    console.log(fp.add(5)(fp.add(10, fp.add(15)(20))))
  })()

  // * NOTE: Compose vs. Chain Functions
  ;(() => {
    type XRepeat = { [key: number]: string }

    function mapRepeatXByNumber(acc: XRepeat, num: number) {
      return {
        ...acc,
        [num]: 'X'.repeat(num)
      }
    }

    const numbers = [10, 20, 30]

    // lodash/fp
    const f = fp.map(fp.add(2))
    const g = fp.filter<number>((x) => x > 20)
    const h = fp.reduce<number, XRepeat>(mapRepeatXByNumber, {})
    const composedFunctions = fp.compose(h, g, f)

    // lodash
    const result = _(numbers)
      .map(function add(x) {
        return _.add(2, x)
      })
      .filter((x) => x > 20)
      .reduce(mapRepeatXByNumber, {})

    console.log('lodash/fp', composedFunctions(numbers))
    console.log('lodash', result)
  })()
})()
