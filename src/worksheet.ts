import MyFp from './mylib'
import { compose } from './mylib/compose'
import { Maybe } from './mylib/functor'
import { filter, map, reduce } from './mylib/list-operators'
import { head, not, tail } from './mylib/operators'

const text =
  'To compose two functions together, pass the \
  output of the first function call as the input of the \
  second function call.'

const fp = new MyFp()
const shortUniqueWords = fp.compose(
  unique,
  getShortWords,
  getShortWords,
  getShortWords,
  unique,
  words
)
console.log(shortUniqueWords(text))

function getShortWords(words: string[]) {
  return words.filter((word) => word.length <= 5)
}

function words(str: string) {
  return str
    .toLowerCase()
    .split(/\s|\b/)
    .filter(function alpha(v) {
      return /^[\w]+$/.test(v)
    })
}

function unique<T>(list: T[]) {
  const uniqList: T[] = []

  for (let v of list) {
    if (uniqList.indexOf(v) === -1) {
      uniqList.push(v)
    }
  }

  return uniqList
}

function calculateSomething(x: number) {
  if (x === 100) return null
  return 50 / (x - 100) + 20
}

Maybe(calculateSomething(100)).isNothing() //?

const a: number = 100
const b = calculateSomething(a)
const c = b === null ? b : calculateSomething(b)
const d = c === null ? c : calculateSomething(c)
const ans = d === null ? d : calculateSomething(d) //?

if (ans) {
  console.log(`The answer is ${ans}`)
} else {
  console.error('no answer')
}

const answer = Maybe(101)
  .map(calculateSomething)
  .map(calculateSomething)
  .map(calculateSomething)
  .map(calculateSomething) //?
if (!answer.isNothing()) {
  console.log(answer.value)
} else {
  console.error('No Answer')
}

function sum(x: number, y: number) {
  return x + y
}
function double(x: number) {
  return x * 2
}
function isOdd(x: number) {
  return x % 2 === 1
}
const composedFn = compose(
  reduce(sum, 0),
  map(double),
  filter(isOdd)
)
composedFn([1,2,3,4,5]) //?

const comped = compose(map((x: number) => x * 2), filter(isOdd))([1,2,3,4,5]) //?

const onlyEven = filter(not(isOdd))

function sumList(x: number, ...xs: number[]) {
  if (xs.length === 0) return x
  return x + sumList(...xs as [number])
}

const input = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
const evens = onlyEven(input) //?
sumList(...evens as [number]) //?

// TEST Proper Tail Call
function factorial(n: number, acc: number = 1): number {
  if (n === 0) return acc * 1
  return factorial(n-1, acc * n)
}

// console.log(factorial(6))

// function zip<T, U>(arr1: T[], arr2: U[], zipped: [T, U][] = []): [T, U][] {
//   if (arr1.length === 0 || arr2.length === 0) return zipped
//   console.log([head(arr1), head(arr2)])
//   return zip(tail(arr1), tail(arr2), [...zipped, [head(arr1), head(arr2)]])
// }
// console.log(zip([1,2,3], ['a','b','c', 'd'])) // [[a,1], [b,2], [c,3]]

// function zip<T, U>(arr1: T[]) {
//   return function forArr2(arr2: U[]): [T, U][] {
//     if (arr1.length === 0 || arr2.length === 0) return []
//     return [[head(arr1), head(arr2)], ...zip<T, U>(tail(arr1))(tail(arr2))]
//   }
// }
// console.log(zip(['a','b','c', 'd','e'])([1,2,3])) // [[a,1], [b,2], [c,3]]


function zip<T, U>(arr1: T[]) {
  return function forArr1(arr2: U[]) {
    return function appliedTail(zipped: [T, U][] = []): [T, U][] {
      if (arr1.length === 0 || arr2.length === 0) return zipped
      return zip<T, U>(tail(arr1))(tail(arr2))([...zipped, [head(arr1), head(arr2)]])
    }
  }
}

console.log(zip(['a','b','c', 'd','e'])([1,2,3])()) // [[a,1], [b,2], [c,3]]